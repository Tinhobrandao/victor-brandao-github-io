<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dados para Cadastro</title>
    <style> h1,
h2,
h3,
h4,
h5,
h6,
p,
label {
    color: snow;
    font-family: Arial, Helvetica, sans-serif;
}

body {
    background-image: url(https://i.pinimg.com/originals/fa/dc/b2/fadcb24075acb650de29f258af69d830.gif);
    background-repeat: no-repeat;
    background-size: 100%;
}

fieldset {
    background-position: 0%;
    background-repeat: no-repeat;
    background-size: cover;
    height: auto;
    width: 15vw;
    background-attachment: scroll;
    opacity: 80%;
    border: thin;
}

input:hover,
select:playing {
    background-image: linear-gradient(to bottom, rgb(219, 206, 206), rgb(214, 171, 214));
    height: 0, 5vh;
    opacity: 50%;
    border: 5px solid rgb(180, 180, 180);
    margin: 10px 10px 10px 10px;
    border-style: outset;
    font-family: Arial, Helvetica, sans-serif;
    color: rgb(255, 255, 255);
}

button:hover {
    background-image: linear-gradient(to bottom, rgb(138, 125, 125), rgb(214, 171, 214));
}

::placeholder {
    color: azure;
}
</style>
</head>
<center>

    <body>
        <main>
            <form>
                <fieldset>
                    <h3>Dados para Login</h3>
                    <label for="Email">Email</label> <br>
                    <input type="text" id="Email" required><br>

                    <label for="password">Senha</label> <br>
                    <input type="password" id="password" ; required> <br>

                    <label for="cpassword">Confirmar senha</label> <br>
                    <input type="password" id="cpassword" required> <br>
                </fieldset>
                <fieldset>
                    <h3>Dados Pessoais</h3>
                    <label for="name">Nome:</label> <br>
                    <input type="text" name="Nome" id="Nome" required> </input> <br>

                    <label for="surname">Sobrenome:</label> <br>
                    <input type="text" name="surname" id="surname" required></input> <br>

                    <label for="sexo">Selecione seu sexo:</label> <br>
                    <input list="gêneros" name="sexo" id="sexo" required>
                    <datalist id="gêneros">
                        <option value="Masculino">Masculino</option>
                        <option value="Feminino">Feminino</option>
                        <option value="Prefiro não informar">Prefiro não informar</option>
                        <option value="Outro">(Digite)</option>
                    </datalist>
                    </input> <br>

                    <label for="CPF">CPF:</label> <br>
                    <input type="text" name="CPF" id="CPF" required> </input> <br>

                    <label for="Telefone">Telefone:</label> <br>
                    <input type="tel" name="Telefone" id="Telefone" placeholder="Ex: (13)99630-6300" required></input>
                    <br>

                    <label for="nasc">Data de Nascimento:</label> <br>
                    <input type="date" name="nasc" id="nasc" required> </input> <br>
                </fieldset>
                <fieldset>
                    <h3>Endereço</h3>
                    <label for="cep">Cep:</label> <br>
                    <input type="text" placeholder="Ex:11346-250" name="cep" id="cep" required> </input> <br>
                    <label for="Rua">Rua</label> <br>
                    <input type="text" name="Rua" id="Rua" required></input> <br>
                    <label for="Número">Número:</label> <br>
                    <input type="text" name="Número" id="Número" required> </input> <br>
                    <label for="Bairro">Bairro:</label> <br>
                    <input type="text" name="Bairro" id="Bairro" required> </input> <br>
                    <p>Selecione seu Estado</p>
                    <select id="estado" name="estado">
                        <option value="AC">Acre</option>
                        <option value="AL">Alagoas</option>
                        <option value="AP">Amapá</option>
                        <option value="AM">Amazonas</option>
                        <option value="BA">Bahia</option>
                        <option value="CE">Ceará</option>
                        <option value="DF">Distrito Federal</option>
                        <option value="ES">Espírito Santo</option>
                        <option value="GO">Goiás</option>
                        <option value="MA">Maranhão</option>
                        <option value="MT">Mato Grosso</option>
                        <option value="MS">Mato Grosso do Sul</option>
                        <option value="MG">Minas Gerais</option>
                        <option value="PA">Pará</option>
                        <option value="PB">Paraíba</option>
                        <option value="PR">Paraná</option>
                        <option value="PE">Pernambuco</option>
                        <option value="PI">Piauí</option>
                        <option value="RJ">Rio de Janeiro</option>
                        <option value="RN">Rio Grande do Norte</option>
                        <option value="RS">Rio Grande do Sul</option>
                        <option value="RO">Rondônia</option>
                        <option value="RR">Roraima</option>
                        <option value="SC">Santa Catarina</option>
                        <option value="SP">São Paulo</option>
                        <option value="SE">Sergipe</option>
                        <option value="TO">Tocantins</option>
                        <option value="EX">Estrangeiro</option>
                    </select> <br>
                </fieldset>
                <button type="reset">Limpar Formulário</button>
                <button type="submit">Realizar Cadastro</button>
            </form>
        </main>
    </body>
</html>
